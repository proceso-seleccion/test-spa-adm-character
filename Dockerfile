FROM node:alpine AS builder
WORKDIR /app
COPY . .
RUN npm install && \
    npm run build

FROM nginx:alpine
EXPOSE 8090
COPY --from=builder /app/dist/* /usr/share/nginx/html/
CMD nginx -g 'daemon off;'
