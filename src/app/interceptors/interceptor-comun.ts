import { Injectable, InjectionToken, Inject } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, finalize, timeout } from 'rxjs/operators';
import { MessageService } from 'primeng/api';

export const TIME_RESPONSE = new InjectionToken<number>('defaultTimeout');

@Injectable()
export class InterceptorComun implements HttpInterceptor {
    constructor(@Inject(TIME_RESPONSE) protected defaultTimeout: number,

        private messageService: MessageService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        



        const timeoutValue = Number(request.headers.get('timeout')) || this.defaultTimeout;


        return next.handle(request).pipe(
            timeout(timeoutValue),
            finalize(() => {
            }),
            catchError((err: any) => {
                this.procesarError(err);
                return of(err);
            })
        );
    }




    private procesarError(err: any) {
        if (err instanceof HttpErrorResponse) {
            try {
                if (err.status === 404) {
                    this.messageService.add({ life: 3000, severity: 'error', summary: '', detail: "Resource not found" });
                }
                else if (err.status === 400) {
                    this.messageService.add({ life: 3000, severity: 'error', summary: '', detail: "Bad request" });
                }
                else if (err.status === 0) {
                    this.messageService.add({ life: 3000, severity: 'error', summary: '', detail: "Servicio no disponible" });
                }
                else {
                    this.messageService.add({ life: 3000, severity: 'error', summary: '', detail: "Internal error. please contact with administrator" });
                }
                console.error(err);
            }
            catch (e) {
                this.messageService.add({ life: 3000, severity: 'error', summary: '', detail: e });
                console.error(e);
            }
        } else {
            this.messageService.add({ life: 3000, severity: 'error', summary: '', detail: "Internal error. please contact with administrator" });
            console.error(err);
        }
    }
}