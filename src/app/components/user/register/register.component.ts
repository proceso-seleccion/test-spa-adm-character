import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { UserDto } from 'src/app/model/user-dto';
import { UserService } from 'src/app/services/user.service';
import { FormUtilComponent } from '../../form-util/form-util.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent extends FormUtilComponent implements OnInit {

  public frmUser = this.formBuilder.group({
    txtEmail: ['', Validators.required],
    txtPassword: ['', Validators.required],
    txtName: ['', Validators.required],
  });

  public userDto: UserDto;

  constructor(public messageService: MessageService, public userService: UserService, private formBuilder: FormBuilder) { 
    super(messageService);
  }

  ngOnInit(): void {
    this.cleanForm();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.frmUser.controls;
  }
  public cleanForm(): void {
    this.userDto = new UserDto;
    this.frmUser.reset();
  }

  private loadUser(): void {
    this.userDto.email = this.f['txtEmail'].value;
    this.userDto.password = this.f['txtPassword'].value;
    this.userDto.name = this.f['txtName'].value;
    this.userDto.status = true;
  }

  public saveUser(): void {
    this.loadUser();
    this.userService.save(this.userDto).subscribe(res => {
      this.putMessageInfo("update record");
      this.cleanForm();
      this.userService.openModalUser = false;
    });
  }

}
