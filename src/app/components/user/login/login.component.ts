import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { sha512 } from 'js-sha512';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public frmLogin = this.formBuilder.group({
    txtEmail: ['', Validators.required],
    txtPassword: ['', Validators.required],
  });
  constructor(public userService: UserService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
  }
  get f(): { [key: string]: AbstractControl } {
    return this.frmLogin.controls;
  }

  public login(): void {
    const email: string = this.f['txtEmail'].value;
    const passwd: string = sha512(this.f['txtPassword'].value);
    this.userService.getLogin(email, passwd).subscribe(res => {
      this.userService.user = res.user;
      this.userService.isLogin = true;
      this.userService.callCharacterFavoriteByIdUser();
      this.clear();
      this.userService.openModalLogin = false;
    });
  }

  public clear(): void {
    this.frmLogin.reset();
  }
}
