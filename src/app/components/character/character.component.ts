import { Component, OnInit } from '@angular/core';
import { CharacterDto } from 'src/app/model/character-dto';
import { ItemDto } from 'src/app/model/item-dto';
import { UserCharacterFavoriteDto } from 'src/app/model/user-character-favorite-dto';
import { UserDto } from 'src/app/model/user-dto';
import { CharacterService } from 'src/app/services/character.service';
import { UserService } from 'src/app/services/user.service';
import { FormUtilComponent } from '../form-util/form-util.component';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent extends FormUtilComponent implements OnInit {

  public characters: CharacterDto;
  public modal:boolean;
  public item:ItemDto;

  constructor(public messageService:MessageService,private characterService: CharacterService, public userService:UserService) {
    super(messageService);
   }

  ngOnInit(): void {
    this.characterService.getAllCharacters().subscribe(res => {
      this.characters = res;
    });
  }

  public apareceModal(item:ItemDto) {
    this.characterService.getCharactersById(item.id).subscribe(res=>{
      this.modal = true;
      this.item = res;
    });
  }

  public addFavorite(item:ItemDto):void {
    const userCharcter = new UserCharacterFavoriteDto;
    userCharcter.idCharacter=Number(item.id);
    userCharcter.character = JSON.stringify(item);
    userCharcter.status = true;
    userCharcter.user= this.userService.user;
    this.userService.saveFavorite(userCharcter).subscribe(res=>{
      this.putMessageInfo("update record.");
      this.userService.callCharacterFavoriteByIdUser();
    })
  }

}
