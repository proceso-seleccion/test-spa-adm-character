import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-form-util',
  templateUrl: './form-util.component.html',
})
export class FormUtilComponent  {

  public onlyLetter:RegExp = /^[A-Z /s]+$/i

  constructor(public messageService: MessageService) { }


  protected putMessageInfo(mensaje: string): void {
      this.messageService.add({ life: 3000, severity: 'success', summary: null, detail: mensaje });
  }

  protected putMessageError(mensaje: string): void {
      this.messageService.add({ life: 3000, severity: 'error', summary: null, detail: mensaje });
  }


}
