import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { UserService } from 'src/app/services/user.service';
import { FormUtilComponent } from '../form-util/form-util.component';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent extends FormUtilComponent implements OnInit {

  items: MenuItem[];
  itemUser: MenuItem[];

  constructor(public messageService:MessageService,public userService: UserService) { 
    super(messageService);
  }

  ngOnInit(): void {
    this.items = [
      {
        label: 'User',
        items: [{
          label: 'Register',
          icon: 'pi pi-user-plus',
          command: () => {
            if (!this.userService.user) {
              this.userService.openModalUser = true;
            } else {
              this.putMessageError("Operation no permit");
            }
          }
        },
        {
          label: 'Login',
          icon: 'pi pi-sign-in',
          command: () => {
            if (!this.userService.user) {
              this.userService.openModalLogin = true;
            } else {
              this.putMessageError("Operation no permit");
            }
          }
        }
        ]
      }
    ];

    this.itemUser = [
      {
        label: 'Options',
        items: [
          {
            label: 'Logout',
            icon: 'pi pi-sign-out',
            command: () => {
              this.userService.user = undefined;
              this.userService.isLogin = true;
            }
          }
        ]
      }
    ];


  }

}
