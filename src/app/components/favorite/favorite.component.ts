import { Component, OnInit } from '@angular/core';
import { FormUtilComponent } from '../form-util/form-util.component';
import { MessageService } from 'primeng/api';
import { UserService } from 'src/app/services/user.service';
import { ResponseDto } from 'src/app/model/response-dto';
import { UserCharacterFavoriteDto } from 'src/app/model/user-character-favorite-dto';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent extends FormUtilComponent implements OnInit {

  constructor(public messageService:MessageService, public userService:UserService) {
    super(messageService);
   }

  ngOnInit(): void {
  }


  public deleteFavorite(item:UserCharacterFavoriteDto) :void {
    item.status=false;
    item.person = undefined;
    this.userService.patchCharacterFavoriteById(item.id,item).subscribe(res=>{
      this.putMessageInfo("update record.");
      this.userService.callCharacterFavoriteByIdUser();
    });
  }
}
