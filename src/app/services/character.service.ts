import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CharacterDto } from '../model/character-dto';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ItemDto } from '../model/item-dto';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private http: HttpClient) { }

  public getAllCharacters(): Observable<CharacterDto> {
    return this.http.get<CharacterDto>(environment.url + "/characters");
  }

  public getCharactersById(id: string): Observable<ItemDto> {
    return this.http.get<ItemDto>(environment.url + "/characters/" + id);
  }
}
