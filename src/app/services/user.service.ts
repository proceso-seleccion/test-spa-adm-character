import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserDto } from '../model/user-dto';
import { ResponseDto } from '../model/response-dto';
import { UserCharacterFavoriteDto } from '../model/user-character-favorite-dto';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public user: UserDto;
  public responseDto: ResponseDto;

  public openModalLogin: boolean;
  public openModalUser: boolean;
  public isLogin: boolean;

  constructor(private http: HttpClient) { }


  public getLogin(email: string, passwd: string): Observable<ResponseDto> {
    return this.http.get<ResponseDto>(environment.url + "/users/" + email + "/" + passwd);
  }

  public save(userDto: UserDto): Observable<UserDto> {
    return this.http.post<UserDto>(environment.url + "/users", userDto);
  }

  public saveFavorite(UserCharacterFavoriteDto: UserCharacterFavoriteDto): Observable<ResponseDto> {
    return this.http.post<ResponseDto>(environment.url + "/usersCharacters", UserCharacterFavoriteDto);
  }

  public getCharacterFavoriteByIdUser(idUser: number): Observable<ResponseDto> {
    return this.http.get<ResponseDto>(environment.url + "/usersCharacters/" + idUser);
  }

  public patchCharacterFavoriteById(id: number, userCharcter:UserCharacterFavoriteDto): Observable<ResponseDto> {
    return this.http.patch<ResponseDto>(environment.url + "/usersCharacters/" + id,userCharcter);
  }

  public callCharacterFavoriteByIdUser(): void {
    this.getCharacterFavoriteByIdUser(this.user.id).subscribe(res => {
      this.responseDto = res;
      this.responseDto.userCharacterFavoriteList.forEach(usr=>{
        usr.person = JSON.parse(usr.character);
      });
    });
  }
}
