import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {PanelModule} from 'primeng/panel';
import {MenuModule} from 'primeng/menu';
import {ButtonModule} from 'primeng/button';
import {ToolbarModule} from 'primeng/toolbar';
import {DataViewModule} from 'primeng/dataview';
import {DialogModule} from 'primeng/dialog';
import {InputTextModule} from 'primeng/inputtext';
import {PasswordModule} from 'primeng/password';
import { MessageService } from 'primeng/api';
import {KeyFilterModule} from 'primeng/keyfilter';
import {ToastModule} from 'primeng/toast';
import {TabViewModule} from 'primeng/tabview';
import {TableModule} from 'primeng/table';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrincipalComponent } from './components/principal/principal.component';
import { CharacterComponent } from './components/character/character.component';
import { LoginComponent } from './components/user/login/login.component';
import { RegisterComponent } from './components/user/register/register.component';
import { InterceptorComun, TIME_RESPONSE } from './interceptors/interceptor-comun';
import { FormUtilComponent } from './components/form-util/form-util.component';
import { FavoriteComponent } from './components/favorite/favorite.component';

@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    CharacterComponent,
    LoginComponent,
    RegisterComponent,
    FormUtilComponent,
    FavoriteComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ToolbarModule,
    TableModule,
    MenuModule,
    InputTextModule,
    PasswordModule,
    KeyFilterModule,
    FormsModule,
    ToastModule,
    TabViewModule,
    DataViewModule,
    BrowserAnimationsModule,
    PanelModule,
    DialogModule,
    ButtonModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [MessageService,
    [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: InterceptorComun,
        multi: true
      }
    ],
    [
      { provide: TIME_RESPONSE, useValue: 30000 }
    ]],
  bootstrap: [AppComponent]
})
export class AppModule { }
