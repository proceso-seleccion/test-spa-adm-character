import { PlanetDto } from "./planet-dto";

export class ItemDto {

    public id: string;
    public name: string;
    public ki: string;
    public maxKi: string;
    public race: string;
    public gender: string;
    public description: string;
    public image: string;
    public affiliation: string;
    public deletedAt: string;
    public originPlanet: PlanetDto;


}
