import { UserDto } from "./user-dto";

export class UserCharacterFavoriteDto {

    public id: number;
    public idCharacter: number;
    public character: string;
    public status: boolean;
    public user: UserDto;
    public person:any;

}
