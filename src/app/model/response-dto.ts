import { UserCharacterFavoriteDto } from "./user-character-favorite-dto";
import { UserDto } from "./user-dto";

export class ResponseDto {

    public message: string;
    public user: UserDto;
    public userCharacterFavoriteList: UserCharacterFavoriteDto[];

}
